asgiref==3.5.2
atomicwrites==1.4.1
attrs==22.1.0
black==22.6.0
click==8.1.3
colorama==0.4.5
coverage==6.4.2
cssbeautifier==1.14.4
Django==4.0.6
djlint==1.9.1
EditorConfig==0.12.3
flake8==4.0.1
html-tag-names==0.1.2
html-void-elements==0.1.0
importlib-metadata==4.12.0
iniconfig==1.1.1
jsbeautifier==1.14.4
mccabe==0.6.1
mypy-extensions==0.4.3
packaging==21.3
pathspec==0.9.0
platformdirs==2.5.2
pluggy==1.0.0
py==1.11.0
pycodestyle==2.8.0
pyflakes==2.4.0
pyparsing==3.0.9
pytest==7.1.2
pytest-cov==3.0.0
PyYAML==6.0
regex==2022.7.25
six==1.16.0
sqlparse==0.4.2
tomli==2.0.1
tqdm==4.64.0
tzdata==2022.1
zipp==3.8.1
